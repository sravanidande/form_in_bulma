import { hello } from './hello';

export class Point {
  x = 1;
  y = 2;
}

console.log(hello(), ...[1, 2, 3], new Point());
