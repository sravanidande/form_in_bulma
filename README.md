# Web Starter

Web development using [parcel](https://parceljs.org/), [eslint](http://eslint.org/) for linting Javascript, [tslint](https://palantir.github.io/tslint/) for linting Typescript, [prettier](https://prettier.io/) for formatting Javascript/Typescript files.

## Usage

1.  Install [nodejs](https://nodejs.org/en/) and [vscode](https://code.visualstudio.com/), preferrably using the following command.

    On Mac

        curl -L j.mp/srtpdtf > setup && bash setup nodefaults web vscode

    On Ubuntu

        wget -qO- j.mp/srtpdtf > setup && bash setup nodefaults web vscode

2.  Clone this repository, install npm packages and open the project in visual studio code. Make sure you have [git](https://git-scm.com/) installed.

        git clone https://gitlab.com/pervezfunctor/web-starter.git
        npm install -g yarn
        cd web-starter
        yarn install
        code .

3.  Start server using the following command.

        yarn start

## License

MIT
